package ru.echodc.homework_2;

import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

  private FragmentManager fragmentManager;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    fragmentManager = getSupportFragmentManager();
    fragmentManager.beginTransaction()
        .replace(R.id.container, MainFragment.newInstance())
        .addToBackStack(MainFragment.class.getName())
        .commit();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater menuInflater = getMenuInflater();
    menuInflater.inflate(R.menu.main_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    switch (item.getItemId()) {
      case R.id.action_settings:
        showMessage(R.string.menu_settings);
        fragmentManager.beginTransaction()
            .replace(R.id.container, SettingFragment.newInstance())
            .addToBackStack(SettingFragment.class.getName())
            .commit();
        break;
      case R.id.action_search:
        showMessage(R.string.menu_search);
        fragmentManager.beginTransaction()
            .replace(R.id.container, SearchFragment.newInstance())
            .addToBackStack(SearchFragment.class.getName())
            .commit();
        break;
      case R.id.action_exit:
        showMessage(R.string.menu_exit);
        finish();
        break;
      default:
        break;
    }
    return super.onOptionsItemSelected(item);
  }

  private void showMessage(@StringRes int msg) {
    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onBackPressed() {
    int count = fragmentManager.getBackStackEntryCount();
    if (count == 0) {
      super.onBackPressed();
      finish();
    } else {
      fragmentManager.popBackStack();
    }
  }
}
