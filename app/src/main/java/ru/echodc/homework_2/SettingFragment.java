package ru.echodc.homework_2;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

public class SettingFragment extends Fragment implements OnClickListener {

  public static final String ENGINE_SEARCH_KEY = "ENGINE_SEARCH_KEY";

  private RadioGroup mRadioGroup;
  private Button mButtonSave;

  private SharedPreferences mSharedPreferences;

  public static SettingFragment newInstance() {
    return new SettingFragment();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.fragment_setting, container, false);

    mSharedPreferences = getActivity().getSharedPreferences("Setting", Context.MODE_PRIVATE);

    mRadioGroup = view.findViewById(R.id.radio_settings_group);
    mButtonSave = view.findViewById(R.id.btn_save);

    mButtonSave.setOnClickListener(this);

    loadSetting();

    return view;
  }

  private void loadSetting() {

    if (mSharedPreferences != null) {

      int checkedRadioButtonId = mSharedPreferences.getInt("ENGINE_SEARCH_KEY", 0);

      mRadioGroup.check(checkedRadioButtonId);

    } else {
      Toast.makeText(getActivity(), "Поиск по умолчанию не выбран!", Toast.LENGTH_LONG).show();
    }
  }

  public void saveRadioValue(View view) {

    SharedPreferences.Editor editor = mSharedPreferences.edit();

    int checkedRadioButtonId = mRadioGroup.getCheckedRadioButtonId();

    editor.putInt(ENGINE_SEARCH_KEY, checkedRadioButtonId);

    editor.apply();

    Toast.makeText(getActivity(), "Setting saved!", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onClick(View view) {
    saveRadioValue(view);
  }
}
