package ru.echodc.homework_2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SearchFragment extends Fragment implements OnClickListener {

  private EditText mSearch;
  private Button mButtonSearch;

  private SharedPreferences mSharedPreferences;

  public static SearchFragment newInstance() {
    return new SearchFragment();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.fragment_search, container, false);
    mSearch = view.findViewById(R.id.et_search);
    mButtonSearch = view.findViewById(R.id.btn_search);

    mButtonSearch.setOnClickListener(this);

    return view;
  }

  @Override
  public void onClick(View view) {
    if (TextUtils.isEmpty(mSearch.getText())) {
      Toast.makeText(getActivity(), "Введите текст для поиска!", Toast.LENGTH_SHORT).show();
    } else {
      startActivity(getIntentForBrowser(mSearch.getText().toString()));
    }
  }

  private Intent getIntentForBrowser(String searchText) {
    return new Intent(Intent.ACTION_VIEW)
        .setData(configureUrl(searchText));
  }

  private Uri configureUrl(String searchText) {
    mSharedPreferences = getActivity().getSharedPreferences("Setting", Context.MODE_PRIVATE);
    int checkedRadio = mSharedPreferences.getInt("ENGINE_SEARCH_KEY", 0);
    //    int checkedRadio = PreferenceManager.getDefaultSharedPreferences(getContext()).getInt("ENGINE_SEARCH_KEY", 0);
    String url = "";
    switch (checkedRadio) {
      case R.id.radio_btn_google: // google
        url = "https://google.com/search?query=" + searchText;
        break;
      case R.id.radio_btn_yandex: // yandex
        url = "https://yandex.ru/search/?query=" + searchText;
        break;
      case R.id.radio_btn_bing: // bing
        url = "https://bing.com/search?query=" + searchText;
        break;
    }
    return Uri.parse(url);
  }
}
